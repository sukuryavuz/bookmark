package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class BookmarkTest {

    @BeforeAll
    public static void classSetUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a AdressDirectory class method and takes place before any @Test is executed");
    }

    @AfterAll
    public static void classTearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a AdressDirectory class method and takes place after all @Test are executed");
    }

    @BeforeEach
    public void setUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place before each @Test is executed");
    }

    @AfterEach
    public void tearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place after each @Test is executed");
    }

    @Test
    @Disabled
    public void failingTest() {
        fail("a disabled failing test");
    }

    @Test
    public void increaseRatingTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 1;
        Bookmark mark = new Bookmark();
        int result = mark.increaseRating();
        assertEquals(expectedResult, result);
    }

    @Test
    public void tagBookmarkTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 2;
        Bookmark mark = new Bookmark();
        mark.tagBookmark("news");
        mark.tagBookmark("entertainment");
        int result = mark.getTags().size();
        assertEquals(expectedResult, result);
    }

    @Test
    public void removeBookmarkTest(){
        int expectedResult = 99;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("http://www.youtube.com");
        ad.createBookmark("http://www.facebook.com");

        assertEquals(-99, ad.removeBookmark("http://www.google.com"));
        assertEquals(expectedResult, ad.removeBookmark("http://www.youtube.com"));
        assertEquals(ad.bookmarks.size(), 1);
    }

}
