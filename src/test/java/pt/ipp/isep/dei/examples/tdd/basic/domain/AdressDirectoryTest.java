package pt.ipp.isep.dei.examples.tdd.basic.domain;

import org.junit.jupiter.api.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class AdressDirectoryTest {

    @BeforeAll
    public static void classSetUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a AdressDirectory class method and takes place before any @Test is executed");
    }

    @AfterAll
    public static void classTearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "This is a AdressDirectory class method and takes place after all @Test are executed");
    }

    @BeforeEach
    public void setUp() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place before each @Test is executed");
    }

    @AfterEach
    public void tearDown() {
        //HACK: for demonstration purposes only
        System.out.println(
                "\tThis call takes place after each @Test is executed");
    }

    @Test
    @Disabled
    public void failingTest() {
        fail("a disabled failing test");
    }


    @Test
    public void createBookmarkTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 0;
        AdressDirectory ad = new AdressDirectory();
        Integer result = ad.createBookmark("https://www.orf.at");

        assertEquals(expectedResult, result);
    }

    @Test
    public void createNonValidBookmarkTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = -99;
        AdressDirectory ad = new AdressDirectory();
        Integer result = ad.createBookmark("orf");

        assertEquals(expectedResult, result);
    }

    @Test
    public void createSameBookmarkTwice() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 1;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.orf.at");
        int result = ad.bookmarks.size();
        int resultrating = ad.bookmarks.get("https://www.orf.at").getRating();
        assertEquals(expectedResult, result);
        assertEquals(expectedResult, resultrating);
    }

    @Test
    public void tagUrlTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 99;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        Integer result = ad.tagURL("https://www.orf.at", "news");
        assertEquals(expectedResult, result);
    }

    @Test
    public void tagWrongUrlTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = -99;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        Integer result = ad.tagURL("https://www.orffff.at", "news");
        assertEquals(expectedResult, result);
    }

    @Test
    public void findSecureUrlsTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 3;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.orf2.at");
        ad.createBookmark("https://www.orf3.at");
        ad.createBookmark("http://www.zib.at");
        Integer result = ad.findSecureURLs().size();
        assertEquals(expectedResult, result);
    }

    @Test
    public void findInSecureUrlsTest() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 1;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.orf2.at");
        ad.createBookmark("https://www.orf3.at");
        ad.createBookmark("http://www.zib.at");
        Integer result = ad.findInSecureURLs().size();
        assertEquals(expectedResult, result);
    }


    @Test
    public void filterUrlWithOneTag() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 2;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.orf2.at");
        ad.createBookmark("https://www.orf3.at");
        ad.createBookmark("http://www.zib.at");
        ad.createBookmark("http://www.youtube.com");

        ad.tagURL("https://www.orf.at", "news");
        ad.tagURL("http://www.zib.at", "news");
        ad.tagURL("http://www.youtube.com", "entertainment");
        Integer result = ad.filterURL("news").size();
        assertEquals(expectedResult, result);
    }

    @Test
    public void filterUrlWithMultipleTags() {
        System.out.println("\t\tExecuting " + new Object() {
        }.getClass().getEnclosingMethod().getName() + " Test");

        int expectedResult = 3;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.orf2.at");
        ad.createBookmark("https://www.orf3.at");
        ad.createBookmark("http://www.zib.at");
        ad.createBookmark("http://www.youtube.com");

        ad.tagURL("https://www.orf.at", "news");
        ad.tagURL("http://www.zib.at", "news");
        ad.tagURL("http://www.youtube.com", "entertainment");
        ArrayList<String> tags = new ArrayList<>();
        tags.add("news");
        tags.add("entertainment");
        Integer result = ad.filterURL2(tags).size();
        assertEquals(expectedResult, result);
    }

    @Test
    public void removeTagFromUrlTest(){
        int expectedResult = 99;
        AdressDirectory ad = new AdressDirectory();
        ad.createBookmark("http://www.youtube.com");

        ad.tagURL("http://www.youtube.com", "entertainment");
        ad.tagURL("http://www.youtube.com", "news");

        assertEquals(-99, ad.removeTag("http://www.facebook.com", "news"));
        assertEquals(expectedResult, ad.removeTag("http://www.youtube.com", "news"));
        assertEquals(ad.bookmarks.get("http://www.youtube.com").getTags().size(), 1);
    }

}




