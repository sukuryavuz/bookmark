package pt.ipp.isep.dei.examples.tdd.basic.ui;

import pt.ipp.isep.dei.examples.tdd.basic.domain.AdressDirectory;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
          AdressDirectory ad = new AdressDirectory();

        ad.createBookmark("https://www.orf.at");
        ad.createBookmark("https://www.zib.at");
        ad.createBookmark("http://www.youtube.com");
        ad.createBookmark("http://www.youtube.com");
        ad.createBookmark("https://instagram.com");

        ad.tagURL("https://www.orf.at", "news");
        ad.tagURL("https://www.zib.at", "news");
        ad.tagURL("http://www.youtube.com", "entertainment");

        System.out.println("Secure URLS = "+ad.findSecureURLs().size());
        System.out.println("Insecure URLS = "+ad.findInSecureURLs().size());

        System.out.println("filtered by one tag");
        for (String url: ad.filterURL("news")) {
            System.out.println(url);
        }

        System.out.println("filtered by multiple tags");
        ArrayList<String> tags = new ArrayList<>();
        tags.add("news");
        tags.add("entertainment");
        for (String url: ad.filterURL2(tags)) {
            System.out.println(url);
        }
    }

}
